# About Soilduino #

Soilduino is a Arduino based soil temperature, moisture, and composition project which takes periodic readings which are uploaded into a C# ASP.NET MVC API application.

For project documentation and system access, please visit [Soilduino](http://www.soilduino.com).

### Branch Structure ###

Active development happens on the develop branch. This branch always contains the latest version. Releases are tagged and moved into the master branch. The master branch will always contain the most stable code available.

### How do I get set up? ###

Soilduino is built against the latest ASP.NET Core 3 for the API service.